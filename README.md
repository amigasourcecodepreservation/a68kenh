# A68kEnh

**Description**:  C. Gibbs "A68k" w/ 68020+ instructions added - an effort to update "A68k".

This was an effort to update A68k with further instructions by Willi Kusche. The work was never fully completed.

  - **Technology stack**: C
  - **Status**:  [CHANGELOG](https://gitlab.com/amigasourcecodepreservation/a68kenh/blob/master/CHANGELOG.md).

**Screenshot**:

![TO-DO](TO-DO)


## Dependencies

* Amiga OS Classic
* Builds with the DICE compiler
* TO-DO...

## Installation

Detailed instructions on how to install, configure, and get the project running.

TO-DO

## Configuration

To build: set up [DICE](http://aminet.net/package/dev/c/dice-3.16), run dmake in the root folder.

TO-DO

## Usage

See [A68k](http://tigcc.ticalc.org/doc/a68k.html)

## How to test the software

TO-DO

## Known issues

TO-DO

## Getting help

If you have questions, concerns, bug reports, etc, please file an issue in this repository's Issue Tracker.

## Getting involved

Contact your old amiga friends and tell them about our project, and ask them to dig out their source code or floppies and send them to us for preservation.

Clean up our hosted archives, and make the source code buildable with standard compilers like devpac, asmone, gcc 2.9x/Beppo 6.x, sas/c ,vbcc and friends.


Cheers!

Twitter
https://twitter.com/AmigaSourcePres

Gitlab
https://gitlab.com/amigasourcecodepreservation

WWW
https://amigasourcepres.gitlab.io/

     _____ ___   _   __  __     _   __  __ ___ ___   _   
    |_   _| __| /_\ |  \/  |   /_\ |  \/  |_ _/ __| /_\  
      | | | _| / _ \| |\/| |  / _ \| |\/| || | (_ |/ _ \ 
     _|_| |___/_/ \_\_|_ |_|_/_/_\_\_|__|_|___\___/_/_\_\
    / __|/ _ \| | | | _ \/ __| __|  / __/ _ \|   \| __|  
    \__ \ (_) | |_| |   / (__| _|  | (_| (_) | |) | _|   
    |___/\___/_\___/|_|_\\___|___|__\___\___/|___/|___|_ 
    | _ \ _ \ __/ __| __| _ \ \ / /_\_   _|_ _/ _ \| \| |
    |  _/   / _|\__ \ _||   /\ V / _ \| |  | | (_) | .` |
    |_| |_|_\___|___/___|_|_\ \_/_/ \_\_| |___\___/|_|\_|

----

## Open source licensing info

A68kWnh is distributed under the terms of the initial Freeware license by Brian R. Anderson. See the [LICENSE](https://gitlab.com/amigasourcecodepreservation/a68kenh/LICENSE.md) file for details.

----

## Credits and references

Many thanks to Willi Kusche for releasing the source code of his work to us.


