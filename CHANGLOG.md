# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [3.0.1-alpha] - 2018-05-010
### Added
- The initial project

### Changed
- Project structure cleanup
- Made the DICE dmakefile build the project
- Integers that were assigned NULL to 0


